import Foundation

extension Date {
   func format(to formatString: String) -> String {
        let dateformat = DateFormatter()
        dateformat.dateFormat = formatString
        return dateformat.string(from: self)
    }
}
