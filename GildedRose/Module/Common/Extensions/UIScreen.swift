import SwiftUI

extension UIScreen {
    static var isSESized: Bool {
        switch UIScreen.main.nativeBounds.height {
        case 1136:
            return true
        default:
            return false
        }
    }
}
