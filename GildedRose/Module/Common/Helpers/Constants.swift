struct Constants {
    static let maxQuality = 50
    static let minQuality = 0
    static let baseQualityModifier = -1
    static let baseSellInModifier = -1
}
