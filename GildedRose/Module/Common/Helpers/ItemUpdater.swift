import Foundation

protocol ItemUpdaterProtocol {
    func update() -> Item
}

struct ItemUpdater: ItemUpdaterProtocol {

    private let item: Item
    private var modifiers: [QualityModifier] {
        getQualityModifier(for: item)
    }

    init(item: Item) {
        self.item = item
    }

    func update() -> Item {

        // Make sure the item is not legendary. If it is, it doesn't need updating.
        guard !modifiers.contains(where: { $0 is LegendaryModifier }) else { return item }

        var currentModifier: Int = Constants.baseQualityModifier

        modifiers.forEach { (modifier) in
            currentModifier = modifier.update(currentModifier, with: item.sellIn)
        }

        let newQualityValue = item.quality + currentModifier

        // make sure the item's quality does not exceed the maximum or minimum values
        item.quality = newQualityValue.clamped(to: Constants.minQuality...Constants.maxQuality)
        item.sellIn = item.sellIn + Constants.baseSellInModifier

        return item
    }

    private func getQualityModifier(for item: Item) -> [QualityModifier] {

        var modifiers: [QualityModifier] = [StandardModifier()]

        if item.name.lowercased().contains("backstage passes") {
            modifiers.append(BackstagePassesModifier())
        }

        if item.name.lowercased().contains("conjured") {
            modifiers.append(ConjuredModifier())
        }

        if item.name.contains(InventoryName.agedBrie.rawValue) {
            modifiers.append(BrieModifier())
        }

        if item.name.contains(InventoryName.sulfuras.rawValue) {
            modifiers.append(LegendaryModifier())
        }

        return modifiers
    }
}
