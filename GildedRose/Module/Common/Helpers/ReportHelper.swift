import Foundation

protocol ReportHelperProtocol {
    func getInventory(for date: Date, with originalInventory: [InventoryItem]) -> [InventoryItem]
}

struct ReportHelper: ReportHelperProtocol {

    func getInventory(for date: Date, with originalInventory: [InventoryItem]) -> [InventoryItem] {

        var mutableOriginalInventory = originalInventory
        let items = mutableOriginalInventory.map { $0.item }
        let app = GildedRose(items: items);

        let startOfToday = Calendar.current.startOfDay(for: Date())
        let days = Calendar.current.dateComponents([.day], from: startOfToday, to: date).day ?? 0

        for _ in 0..<days {
            app.updateQuality();
        }

        app.items.enumerated().forEach {
            mutableOriginalInventory[$0.offset].item = $0.element
        }

        return mutableOriginalInventory
    }
}
