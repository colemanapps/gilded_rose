import Foundation

struct BackstagePassesModifier: QualityModifier {
    func update(_ currentModifier: Int, with sellIn: Int) -> Int {
        if sellIn <= 0 {
            return -Int.max
        } else if (0...5).contains(sellIn) {
            return abs(currentModifier) + 2
        } else if (5...10).contains(sellIn) {
            return abs(currentModifier) + 1
        } else {
            return abs(currentModifier)
        }
    }
}
