import Foundation

struct BrieModifier: QualityModifier {
    func update(_ currentModifier: Int, with sellIn: Int) -> Int {
        return abs(currentModifier)
    }
}
