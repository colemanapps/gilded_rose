import Foundation

struct ConjuredModifier: QualityModifier {
    func update(_ currentModifier: Int, with sellIn: Int) -> Int {
        return currentModifier * 2
    }
}
