import Foundation

struct LegendaryModifier: QualityModifier {
    func update(_ currentModifier: Int, with sellIn: Int) -> Int {
        return currentModifier
    }
}
