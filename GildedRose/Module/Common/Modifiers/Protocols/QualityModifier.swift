import Foundation

protocol QualityModifier {
    func update(_ currentModifier: Int, with sellIn: Int) -> Int
}
