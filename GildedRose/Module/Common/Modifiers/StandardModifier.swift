import Foundation

struct StandardModifier: QualityModifier {
    func update(_ currentModifier: Int, with sellIn: Int) -> Int {
        return sellIn <= 0 ? currentModifier * 2 : currentModifier
    }
}
