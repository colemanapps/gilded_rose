enum InventoryName: String {
    case dexterityVest = "+5 Dexterity Vest"
    case agedBrie = "Aged Brie"
    case elixirOfTheMongoose = "Elixir of the Mongoose"
    case sulfuras = "Sulfuras, Hand of Ragnaros"
    case backstageTAFKAL = "Backstage passes to a TAFKAL80ETC concert"
    case manaCake = "Conjured Mana Cake"
}
