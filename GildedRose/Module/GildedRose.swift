import Foundation

public class GildedRose {

    var items: [Item]

    public init(items:[Item]) {
        self.items = items
    }

    public func updateQuality() {
        items = items.map { ItemUpdater(item: $0).update() }
    }
}
