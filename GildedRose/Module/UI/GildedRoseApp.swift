import SwiftUI

@main
struct GildedRoseApp: App {
    var body: some Scene {
        WindowGroup {
            GildedRoseListView()
        }
    }
}
