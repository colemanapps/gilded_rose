import SwiftUI
import Combine

struct GildedRoseListView: View {

    @State private var showModal = false
    @State private var date = Date()
    @State private var showPicker = false

    @ObservedObject var viewModel: GildedRoseListViewModel = GildedRoseListViewModel()

    private var closedDateRange: ClosedRange<Date> {
        let today = Date()
        let threeWeeks = Calendar.current.date(byAdding: .day, value: 30, to: Date())!
        return today...threeWeeks
    }

    init() {
        UINavigationBar.appearance().largeTitleTextAttributes = [.font : UIFont(name: "Zapfino", size: 20)!]
        UINavigationBar.appearance().titleTextAttributes = [.font : UIFont(name: "Zapfino", size: 12)!]
        UINavigationBar.appearance().backgroundColor = .white
        UINavigationBar.appearance().tintColor = .black
    }

    var body: some View {
        NavigationView {
            VStack(alignment: .leading, spacing: 0) {

                itemList
                    .navigationTitle(Text("The Gilded Rose"))
                    .navigationBarItems(leading: gearButton, trailing: reportButton)

                Text("Showing inventory for: \(date.format(to: "dd MMMM yyyy"))")
                    .font(.custom("Beleren2016SmallCaps-Bold", size: 14))
                    .padding()

                if showPicker {
                    pickerView
                }
            }
            .sheet(isPresented: $showModal) {
                ReportView(isPresented: self.$showModal, inventory: viewModel.inventory)
            }
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }

    private var gearButton: some View {
        Button(action: gearButtonPressed) {
            Image(systemName: "gear")
                .font(Font.headline.weight(.semibold))
        }
    }

    private var reportButton: some View {
        Button(action: reportButtonPressed) {
            Image(systemName: "book")
                .font(Font.headline.weight(.semibold))
        }
    }

    private var itemList: some View {
        List {
            ForEach(viewModel.inventoryToDisplay) { inventoryItem in
                NavigationLink(destination: InventoryItemDetail(inventoryItem: inventoryItem)) {
                    InventoryItemListView(item: inventoryItem.item)
                }
            }
        }
    }

    private var pickerView: some View {
        HStack {
            Spacer()
            DatePicker("", selection: $date, in: closedDateRange, displayedComponents: .date)
                .datePickerStyle(WheelDatePickerStyle())
                .frame(width: CGFloat.leastNormalMagnitude) // Needed to fix a bug on iPhone SE rendering badly
                .labelsHidden()
                .onChange(of: date) { (newDate) in
                    viewModel.updateInventory(for: newDate)
                }
            Spacer()
        }
        .padding(.horizontal)
    }

    private func reportButtonPressed() {
        self.showModal.toggle()
    }

    private func gearButtonPressed() {
        withAnimation { self.showPicker.toggle() }
    }
}

struct GildedRoseListView_Previews: PreviewProvider {
    static var previews: some View {
        GildedRoseListView()
    }
}
