import SwiftUI
import Combine

struct InventoryItem: Identifiable {
    var id = UUID()
    var item: Item
    let description: String
    let imageName: String
}

final class GildedRoseListViewModel: ObservableObject {

    private var reportHelper: ReportHelperProtocol

    var inventory: [InventoryItem] {
        [
            InventoryItem(item: Item(name: "+5 Dexterity Vest", sellIn: 10, quality: 20), description: "A vest worn only by the most nimble of creatures. Wearing this vest boosts your dexterity skill by 5.", imageName: "DexterityVest"),
            InventoryItem(item: Item(name: "Aged Brie", sellIn: 2, quality: 0), description: "This is the stinkiest of cheeses. The longer you leave it, the tastier it gets. Try to resist temptation.", imageName: "AgedBrie"),
            InventoryItem(item: Item(name: "Elixir of the Mongoose", sellIn: 5, quality: 7), description: "This elixir may smell awful, but it will fully refill your health bar!", imageName: "Elixer"),
            InventoryItem(item: Item(name: "Sulfuras, Hand of Ragnaros", sellIn: 0, quality: 80), description: "Sulfuras, Hand of Ragnos. A legendary weapon.", imageName: "Sulfuras"),
            InventoryItem(item: Item(name: "Sulfuras, Hand of Ragnaros", sellIn: -1, quality: 80), description: "Sulfuras, Hand of Ragnos. A legendary weapon.", imageName: "Sulfuras"),
            InventoryItem(item: Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 15, quality: 20), description: "The talk of the town! Get your tickets before they run out. Now with special VIP backstage access.", imageName: "BackstagePass"),
            InventoryItem(item: Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 10, quality: 49), description: "The talk of the town! Get your tickets before they run out. Now with special VIP backstage access.", imageName: "BackstagePass"),
            InventoryItem(item: Item(name: "Backstage passes to a TAFKAL80ETC concert", sellIn: 5, quality: 49), description: "The talk of the town! Get your tickets before they run out. Now with special VIP backstage access.", imageName: "BackstagePass"),
            InventoryItem(item: Item(name: "Conjured Mana Cake", sellIn: 3, quality: 6), description: "This cake looks delicious but be warned! It is a conjured, so it might taste a little ... off.", imageName: "ManaCake")
        ]
    }

    @Published var inventoryToDisplay: [InventoryItem] = []

    init(with reportHelper: ReportHelperProtocol = ReportHelper()) {
        self.reportHelper = reportHelper
        self.inventoryToDisplay = inventory
    }

    func updateInventory(for date: Date) {
        self.inventoryToDisplay = reportHelper.getInventory(for: date, with: inventory)
    }
}
