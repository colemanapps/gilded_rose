import SwiftUI

struct InventoryItemDetail: View {

    let inventoryItem: InventoryItem

    var body: some View {
        HStack {
            Spacer()
            contentView
            Spacer()
        }
    }

    private var contentView: some View {
        GeometryReader { geometry in
            ZStack(alignment: .center) {

                RoundedRectangle(cornerRadius: 25)
                    .fill(Color.black)
                    .frame(width: geometry.size.width, height: geometry.size.height)
                    .shadow(radius: 5)

                ZStack(alignment: .top) {
                    Image("Card_Primary")
                        .resizable()
                        .cornerRadius(8)
                        .frame(width: geometry.size.width * 0.9, height: geometry.size.height * 0.95)

                    VStack(spacing: 0) {
                        titleView(with: geometry, text: inventoryItem.item.name)
                        subView(with: geometry, isImage: true)
                        titleView(with: geometry, text: "Description")
                        subView(with: geometry, isImage: false)
                    }
                }
            }
        }
    }

    private func titleView(with superView: GeometryProxy, text: String) -> some View {

        return ZStack(alignment: .leading) {
            Image("Card_Secondary")
                .resizable()
                .cornerRadius(5, corners: [.topLeft, .topRight])

            Text(text)
                .font(.custom("Beleren2016SmallCaps-Bold", size: UIScreen.isSESized ? 12 : 16))
                .padding(.horizontal)
        }
        .frame(width: superView.size.width * 0.8, height: superView.size.height * 0.05)
        .padding([.leading, .trailing, .top])
    }

    private func subView(with superView: GeometryProxy, isImage: Bool) -> some View {

        return ZStack(alignment: isImage ? .center : .topLeading) {

            Rectangle()
                .fill(Color.white)
                .cornerRadius(5, corners: [.bottomLeft, .bottomRight])

            if isImage {
                Image(inventoryItem.imageName)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
            } else {
                Text(inventoryItem.description)
                    .font(.custom("Beleren2016-Bold", size: UIScreen.isSESized ? 12 : 16))
                    .padding()
            }
        }
        .frame(width: superView.size.width * 0.8, height: superView.size.width * 0.6)
        .padding(.vertical, 0)

    }
}

fileprivate extension View {
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape(RoundedCorner(radius: radius, corners: corners))
    }
}

struct InventoryItemDetail_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            InventoryItemDetail(inventoryItem: InventoryItem(item: Item(name: "TestItem", sellIn: 20, quality: 40), description: "This is a test description of a test item", imageName: "AgedBrie")).previewDisplayName("Smallest")
            InventoryItemDetail(inventoryItem: InventoryItem(item: Item(name: "TestItem", sellIn: 20, quality: 40), description: "This is a test description of a test item", imageName: "AgedBrie")).previewDevice(PreviewDevice(rawValue: "iPhone XS Max")).previewDisplayName("Biggest")
        }
    }
}
