import SwiftUI

struct InventoryItemListView: View {

    let item: Item

    var body: some View {
        HStack {
            Text(item.name)
            Spacer()
            VStack {
                Text("Quality: \(item.quality)")
                Text("Sell by: \(item.sellIn)")
            }
        }
        .font(.custom("Beleren2016-Bold", size: 14))
    }
}
