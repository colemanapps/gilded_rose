import SwiftUI

struct ReportView: View {

    @Binding var isPresented: Bool
    let inventory: [InventoryItem]

    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            Text(generateReportString())
                .font(.custom("Beleren2016-Bold", size: 14))
        }
    }

    private func generateReportString() -> String {
        let items = inventory.map { $0.item }
        let app = GildedRose(items: items);
        let days = 14
        var returnableString: String = ""

        for i in 0..<days {
            returnableString += "\n"
            returnableString += "-------- day \(i) -------- \n"
            returnableString += "name, sellIn, quality \n"
            for inventoryItem in inventory {
                returnableString += "\(inventoryItem.item) \n"
            }
            app.updateQuality();
        }

        return returnableString
    }
}
