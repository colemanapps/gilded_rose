@testable import GildedRose
import XCTest

class GildedRoseTests: XCTestCase {

    let maxQuality = 50
    let minQuality = 0

    ///
    // MARK: Generic tests
    ///
    func test_genericItem_normalDegration() {

        // when we start with an item with a sell by value above 0 and a quality value above 1
        let sellByValue = 10
        let qualityValue = 30
        let item = Item(name: InventoryName.dexterityVest.rawValue, sellIn: sellByValue, quality: qualityValue)

        // when we run an update of the quality
        let app = GildedRose(items: [item])
        app.updateQuality()

        // the sell by value and the quality value are decreased by 1
        XCTAssertEqual(item.sellIn, sellByValue - 1)
        XCTAssertEqual(item.quality, qualityValue - 1)
    }

    func test_genericItem_minQualityDegration() {

        // when we start with an item with a sell by value above 0 and a quality value of zero
        let sellByValue = 10
        let qualityValue = minQuality
        let item = Item(name: InventoryName.dexterityVest.rawValue, sellIn: sellByValue, quality: qualityValue)

        // when we run an update of the quality
        let app = GildedRose(items: [item])
        app.updateQuality()

        // the sell by value decreases by 1 but the quality value does not
        XCTAssertEqual(item.quality, minQuality)
        XCTAssertEqual(item.sellIn, sellByValue - 1)
    }

    func test_genericItem_zeroSellByDegration() {

        // when we start with an item with a sell by value of 0 and a quality value above 1
        let sellByValue = 0
        let qualityValue = 10
        let item = Item(name: InventoryName.dexterityVest.rawValue, sellIn: sellByValue, quality: qualityValue)

        // when we run an update of the quality
        let app = GildedRose(items: [item])
        app.updateQuality()

        // the sell by value decreases by 1 but the quality value decreases by 2
        XCTAssertEqual(item.quality, qualityValue - 2)
        XCTAssertEqual(item.sellIn, sellByValue - 1)
    }

    func test_genericItem_zeroSellBy_singleQualityDegration() {

        // when we start with an item with a sell by value of 0 and a quality value of 1
        let sellByValue = 0
        let qualityValue = 1
        let item = Item(name: InventoryName.dexterityVest.rawValue, sellIn: sellByValue, quality: qualityValue)

        // when we run an update of the quality
        let app = GildedRose(items: [item])
        app.updateQuality()

        // the sell by value decreases by 1 but the quality value decreases to 0
        XCTAssertEqual(item.quality, minQuality)
        XCTAssertEqual(item.sellIn, sellByValue - 1)
    }

    func test_genericItem_conjuredDegration() {

        // when we start with a conjured item with a sell by value above 0 and a quality value above 1
        let sellByValue = 10
        let qualityValue = 30
        let item = Item(name: "Conjured \(InventoryName.dexterityVest.rawValue)", sellIn: sellByValue, quality: qualityValue)

        // when we run an update of the quality
        let app = GildedRose(items: [item])
        app.updateQuality()

        // the sell by value and the quality value are decreased by 2 (due to conjured modifier)
        XCTAssertEqual(item.sellIn, sellByValue - 1)
        XCTAssertEqual(item.quality, qualityValue - 2)
    }

    ///
    // MARK: Aged Brie tests
    ///
    func test_agedBrie_normalIncrease() {

        // when we start with an Aged Brie with a sell by value above 0 and a quality value above 1
        let sellByValue = 10
        let qualityValue = 30
        let item = Item(name: InventoryName.agedBrie.rawValue, sellIn: sellByValue, quality: qualityValue)

        // when we run an update of the quality
        let app = GildedRose(items: [item])
        app.updateQuality()

        // the sell by value is decreased but the quality value is increased by 1
        XCTAssertEqual(item.sellIn, sellByValue - 1)
        XCTAssertEqual(item.quality, qualityValue + 1)
    }

    func test_agedBrie_maxQualityIncrease() {

        // when we start with an Aged Brie with a sell by value above 0 and a quality value of 50
        let sellByValue = 10
        let qualityValue = maxQuality
        let item = Item(name: InventoryName.agedBrie.rawValue, sellIn: sellByValue, quality: qualityValue)

        // when we run an update of the quality
        let app = GildedRose(items: [item])
        app.updateQuality()

        // the sell by value is decreased but the quality value is increased by 1
        XCTAssertEqual(item.sellIn, sellByValue - 1)
        XCTAssertEqual(item.quality, maxQuality)
    }

    func test_agedBrie_zeroSellByIncrease() {

        // when we start with an Aged Brie with a sell by value of 0 and a quality value above 1
        let sellByValue = 0
        let qualityValue = 10
        let item = Item(name: InventoryName.agedBrie.rawValue, sellIn: sellByValue, quality: qualityValue)

        // when we run an update of the quality
        let app = GildedRose(items: [item])
        app.updateQuality()

        // the sell by value decreases by 1 but the quality value increases by 2
        XCTAssertEqual(item.quality, qualityValue + 2)
        XCTAssertEqual(item.sellIn, sellByValue - 1)
    }

    func test_agedBrie_zeroSellBy_nearMaxIncrease() {

        // when we start with an Aged Brie with a sell by value of 0 and a quality value of 1
        let sellByValue = 0
        let qualityValue = 49
        let item = Item(name: InventoryName.agedBrie.rawValue, sellIn: sellByValue, quality: qualityValue)

        // when we run an update of the quality
        let app = GildedRose(items: [item])
        app.updateQuality()

        // the sell by value decreases by 1 but the quality value decreases to 0
        XCTAssertEqual(item.quality, maxQuality)
        XCTAssertEqual(item.sellIn, sellByValue - 1)
    }

    func test_agedBrie_conjuredIncrease() {

        // when we start with a Conjured Aged Brie with a sell by value above 0 and a quality value above 1
        let sellByValue = 10
        let qualityValue = 30
        let item = Item(name: "Conjured \(InventoryName.agedBrie.rawValue)", sellIn: sellByValue, quality: qualityValue)

        // when we run an update of the quality
        let app = GildedRose(items: [item])
        app.updateQuality()

        // the sell by value is decreased but the quality value is increased by 2 (due to conjured modifier)
        XCTAssertEqual(item.sellIn, sellByValue - 1)
        XCTAssertEqual(item.quality, qualityValue + 2)
    }

    ///
    // MARK: Sulfuras tests
    ///
    func test_sulfuras_fixedValues() {

        // when we start with the legendary Sulfuras, Hand of Ragnaros
        let sellByValue = 10
        let qualityValue = 80
        let item = Item(name: InventoryName.sulfuras.rawValue, sellIn: sellByValue, quality: qualityValue)

        // when we run an update of the quality
        let app = GildedRose(items: [item])
        app.updateQuality()

        // the sell by value and quality stay the same
        XCTAssertEqual(item.sellIn, sellByValue)
        XCTAssertEqual(item.quality, qualityValue)
    }

    ///
    // MARK: Backstage passes tests
    ///
    func test_backstagePasses_normalIncrease_15days() {

        // when we start with backstage passes with a sell by value above 10 and a quality value below max
        let sellByValue = 15
        let qualityValue = 30
        let item = Item(name: InventoryName.backstageTAFKAL.rawValue, sellIn: sellByValue, quality: qualityValue)

        // when we run an update of the quality
        let app = GildedRose(items: [item])
        app.updateQuality()

        // the sell by value is decreased but the quality value is increased by 1
        XCTAssertEqual(item.sellIn, sellByValue - 1)
        XCTAssertEqual(item.quality, qualityValue + 1)
    }

    func test_backstagePasses_normalIncrease_10days() {

        // when we start with backstage passes with a sell by value between 5 and 10 and a quality value below max
        let sellByValue = 10
        let qualityValue = 30
        let item = Item(name: InventoryName.backstageTAFKAL.rawValue, sellIn: sellByValue, quality: qualityValue)

        // when we run an update of the quality
        let app = GildedRose(items: [item])
        app.updateQuality()

        // the sell by value is decreased but the quality value is increased by 2
        XCTAssertEqual(item.sellIn, sellByValue - 1)
        XCTAssertEqual(item.quality, qualityValue + 2)
    }

    func test_backstagePasses_normalIncrease_5days() {

        // when we start with backstage passes with a sell by value between 0 and 5 and a quality value below max
        let sellByValue = 5
        let qualityValue = 30
        let item = Item(name: InventoryName.backstageTAFKAL.rawValue, sellIn: sellByValue, quality: qualityValue)

        // when we run an update of the quality
        let app = GildedRose(items: [item])
        app.updateQuality()

        // the sell by value is decreased but the quality value is increased by 3
        XCTAssertEqual(item.sellIn, sellByValue - 1)
        XCTAssertEqual(item.quality, qualityValue + 3)
    }

    func test_backstagePasses_conjuredIncrease_15days() {

        // when we start with backstage passes with a sell by value above 10 and a quality value below max
        let sellByValue = 15
        let qualityValue = 30
        let item = Item(name: "Conjured \(InventoryName.backstageTAFKAL.rawValue)", sellIn: sellByValue, quality: qualityValue)

        // when we run an update of the quality
        let app = GildedRose(items: [item])
        app.updateQuality()

        // the sell by value is decreased but the quality value is increased by 2 (due to conjured modifier)
        XCTAssertEqual(item.sellIn, sellByValue - 1)
        XCTAssertEqual(item.quality, qualityValue + 2)
    }

    func test_backstagePassesOneDirection_normalIncrease_15days() {

        // when we start with backstage passes with a sell by value above 10 and a quality value below max
        let sellByValue = 15
        let qualityValue = 30
        let item = Item(name: "Backstage passes to a One Direction concert", sellIn: sellByValue, quality: qualityValue)

        // when we run an update of the quality
        let app = GildedRose(items: [item])
        app.updateQuality()

        // the sell by value is decreased but the quality value is increased by 1
        XCTAssertEqual(item.sellIn, sellByValue - 1)
        XCTAssertEqual(item.quality, qualityValue + 1)
    }

    func test_backstagePasses_conjuredIncrease_10days() {

        // when we start with backstage passes with a sell by value between 5 and 10 and a quality value below max
        let sellByValue = 10
        let qualityValue = 30
        let item = Item(name: "Conjured \(InventoryName.backstageTAFKAL.rawValue)", sellIn: sellByValue, quality: qualityValue)

        // when we run an update of the quality
        let app = GildedRose(items: [item])
        app.updateQuality()

        // the sell by value is decreased but the quality value is increased by 4 (due to conjured modifier)
        XCTAssertEqual(item.sellIn, sellByValue - 1)
        XCTAssertEqual(item.quality, qualityValue + 4)
    }

    func test_backstagePasses_conjuredIncrease_5days() {

        // when we start with backstage passes with a sell by value between 0 and 5 and a quality value below max
        let sellByValue = 5
        let qualityValue = 30
        let item = Item(name: "Conjured \(InventoryName.backstageTAFKAL.rawValue)", sellIn: sellByValue, quality: qualityValue)

        // when we run an update of the quality
        let app = GildedRose(items: [item])
        app.updateQuality()

        // the sell by value is decreased but the quality value is increased by 6 (due to conjured modifier)
        XCTAssertEqual(item.sellIn, sellByValue - 1)
        XCTAssertEqual(item.quality, qualityValue + 6)
    }

    func test_backstagePasses_normalIncrease_0days() {

        // when we start with backstage passes with a sell by value of 0 and any quality value
        let sellByValue = 0
        let qualityValue = 30
        let item = Item(name: "\(InventoryName.backstageTAFKAL.rawValue)", sellIn: sellByValue, quality: qualityValue)

        // when we run an update of the quality
        let app = GildedRose(items: [item])
        app.updateQuality()

        // the sell by value is decreased but the quality value is reset to minQuality (the concert is in the past)
        XCTAssertEqual(item.sellIn, sellByValue - 1)
        XCTAssertEqual(item.quality, minQuality)
    }
}
